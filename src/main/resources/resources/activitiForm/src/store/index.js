import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import permission from './modules/permission'
import commonData from './modules/commonData'
// import mapModular from './modules/mapModular'
// 主题
// import theme from './modules/theme'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user,
    permission,
    commonData
    // mapModular,
    // theme
  },
  getters
})

export default store
