/**
 * @description 显示菜单logo控制
 * @author zr
 */
// import defaultSettings from '@/settings'

// const {
//   showSettings,
//   showBreadcrumb
// } = defaultSettings

const state = {
  showSettings: true,
  showBreadcrumb: false
}

const mutations = {
  CHANGE_SETTING: (state, { key, value }) => {
    // eslint-disable-next-line no-prototype-builtins
    if (state.hasOwnProperty(key)) {
      state[key] = value
    }
  }
}

const actions = {
  changeSetting({ commit }, data) {
    commit('CHANGE_SETTING', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
