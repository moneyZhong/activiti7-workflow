import setting from '@/settings.js'
const state = {
  list: setting.theme.list,
  activeName: ''
}
const mutations = {
  dom(state) {
    document.body.className = `theme-${state.activeName}`
  }
}
const actions = {
  // 加载主题
  load({ state, commit }) {
    // 检查本地存储是否有主题名称
    const themeActiveName = sessionStorage.getItem('theme-name')
    state.activeName = themeActiveName || (setting.theme.activeName !== '' && setting.theme.activeName ? setting.theme.activeName : state.list[0].themeValue)
    sessionStorage.setItem('theme-name', state.activeName)
    commit('dom')
  },
  // 更换主题
  set({ state, commit }, themeName) {
    // 检查这个主题是否存在
    state.activeName = state.list.find(e => e.themeValue === themeName) ? themeName : state.list[0].themeValue
    sessionStorage.setItem('theme-name', state.activeName)
    commit('dom')
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
