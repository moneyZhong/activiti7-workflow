package com.imooc.activitiweb.controller;


import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.imooc.activitiweb.SecurityUtil;
import com.imooc.activitiweb.mapper.UtilMapper;
import com.imooc.activitiweb.util.AjaxResponse;
import com.imooc.activitiweb.util.GlobalConfig;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/HistoryProcess")
public class HistoryProcessController {


    @Autowired
    private UtilMapper utilMapper;


    @Autowired
    private SecurityUtil securityUtil;

    @GetMapping("/getHistory")
    public AjaxResponse getHistory(@ApiParam(value = "页码")@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum,
                                   @ApiParam(value = "大小")@RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize,
                                   @RequestParam("proc_inst_id") String instanceID){

        try{
//            if (!GlobalConfig.Test) {
//                securityUtil.logInAs("bajie");
//            }
            //根据流程实例(instanceID) 去act_hi_taskinst表中查询 当前实例的所有已结束的任务环节的相关数据（执行人 修改时间 任务环节的名称）
            List<HashMap<String, Object>> historyList = utilMapper.selectHistoryAssigneeAndName(instanceID);


            PageHelper.startPage(pageNum,pageSize);

            PageInfo result = new PageInfo(historyList);

            JSONObject jsonData=new JSONObject();
            jsonData.put("totalNum",historyList.size());
            jsonData.put("pageNum",pageNum);
            jsonData.put("pageSize",pageSize);
            jsonData.put("list",result.getList());
            return AjaxResponse.AjaxData(GlobalConfig.ResponseCode.SUCCESS.getCode(),
                GlobalConfig.ResponseCode.SUCCESS.getDesc(), jsonData);
        }catch (Exception e){
             return AjaxResponse.AjaxData(GlobalConfig.ResponseCode.ERROR.getCode(),
                "获取历史流程失败", e.toString());
        }

    }

}
