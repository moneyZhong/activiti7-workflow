package com.imooc.activitiweb.pojo;

public class FormValue {
    private String taskid;

    private String controlid;

    private String controlval;

    private String procDefId;

    private String procInstId;

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid == null ? null : taskid.trim();
    }

    public String getControlid() {
        return controlid;
    }

    public void setControlid(String controlid) {
        this.controlid = controlid == null ? null : controlid.trim();
    }

    public String getControlval() {
        return controlval;
    }

    public void setControlval(String controlval) {
        this.controlval = controlval == null ? null : controlval.trim();
    }

    public String getProcDefId() {
        return procDefId;
    }

    public void setProcDefId(String procDefId) {
        this.procDefId = procDefId == null ? null : procDefId.trim();
    }

    public String getProcInstId() {
        return procInstId;
    }

    public void setProcInstId(String procInstId) {
        this.procInstId = procInstId == null ? null : procInstId.trim();
    }
}