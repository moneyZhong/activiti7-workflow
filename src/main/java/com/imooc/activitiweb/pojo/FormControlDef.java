package com.imooc.activitiweb.pojo;

public class FormControlDef {
    private String taskid;

    private String controlid;

    private String controltype;

    private String defaultvalue;

    private String controlvalue;

    private Boolean isparams;

    public String getTaskid() {
        return taskid;
    }

    public void setTaskid(String taskid) {
        this.taskid = taskid == null ? null : taskid.trim();
    }

    public String getControlid() {
        return controlid;
    }

    public void setControlid(String controlid) {
        this.controlid = controlid == null ? null : controlid.trim();
    }

    public String getControltype() {
        return controltype;
    }

    public void setControltype(String controltype) {
        this.controltype = controltype == null ? null : controltype.trim();
    }

    public String getDefaultvalue() {
        return defaultvalue;
    }

    public void setDefaultvalue(String defaultvalue) {
        this.defaultvalue = defaultvalue == null ? null : defaultvalue.trim();
    }

    public String getControlvalue() {
        return controlvalue;
    }

    public void setControlvalue(String controlvalue) {
        this.controlvalue = controlvalue == null ? null : controlvalue.trim();
    }

    public Boolean getIsparams() {
        return isparams;
    }

    public void setIsparams(Boolean isparams) {
        this.isparams = isparams;
    }

    @Override
    public String toString() {
        return "FormControlDef{" +
                "taskid='" + taskid + '\'' +
                ", controlid='" + controlid + '\'' +
                ", controltype='" + controltype + '\'' +
                ", defaultvalue='" + defaultvalue + '\'' +
                ", controlvalue='" + controlvalue + '\'' +
                ", isparams=" + isparams +
                '}';
    }
}